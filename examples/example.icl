module example

/**
 * This file is part of iTasks-Resizeable-Split.
 *
 * iTasks-Resizeable-Split is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * iTasks-Resizeable-Split is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Resizeable-Split. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import iTasks
import iTasks.ResizeableSplit

Start w = doTasks main w

main =
	ResizeableSplit @>> ArrangeHorizontal @>> allTasks
	(repeatn 3 (
		ResizeableSplit @>> allTasks
			(repeatn 3 (Title "Subtask" @>> viewInformation [] 37))))
