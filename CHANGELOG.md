# Changelog

#### v1.0.4

- Fix: hook `afterRender` instead of `initDOMEl`.

#### v1.0.3

- Robustness: tentative fix, don't create a new split when there is already one
  (is `initDOMEl` called twice in some cases?).
- Chore: allow iTasks 0.8–0.12.

#### v1.0.2

- Fix: fix horizontal splits; set width to 100%.

#### v1.0.1

- Fix: include CSS.

## v0.1

First tagged release.
