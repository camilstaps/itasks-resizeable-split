# iTasks-Resizeable-Split

A small [iTasks][] utility to make subtasks of a `parallel` individually
resizeable by the user. Between each two tasks a draggable gutter is inserted
using [split.js][].

![Example screenshot](/readme.png)

## Usage

```clean
// Horizontal split:
ResizeableSplit @>> ArrangeHorizontal @>> allTasks [ /* ... */ ]

// Vertical split:
ResizeableSplit @>> allTasks [ /* ... */ ]
```

## Author &amp; License
This library is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits.

This project is licensed under AGPL v3 with additional terms under section 7;
see the [LICENSE](/LICENSE) file for details.

[Camil Staps]: https://camilstaps.nl
[iTasks]: https://clean-lang.org/pkg/itasks
[split.js]: https://split.js.org
