implementation module iTasks.ResizeableSplit

/**
 * This file is part of iTasks-Resizeable-Split.
 *
 * iTasks-Resizeable-Split is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * iTasks-Resizeable-Split is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Resizeable-Split. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv

import Control.Applicative
import Data.Either
import Data.Func
import Data.Functor

import iTasks
import iTasks.UI.JavaScript

import ABC.Interpreter.JavaScript

instance tune ResizeableSplit (Task a)
where
	tune config task =
		AddCSSClass "itasks-resizeable-split" @>>
		JavaScriptInit (addSplit config) @>>
		task

addSplit :: !ResizeableSplit !FrontendEngineOptions !JSVal !*JSWorld -> *JSWorld
addSplit config {FrontendEngineOptions|serverDirectory} me w
	# (afterRender,w) = jsWrapFun (afterRender serverDirectory config me) me w
	# w = (me .# "afterRender" .= afterRender) w
	= w

afterRender :: !String !ResizeableSplit !JSVal !{!JSVal} !*JSWorld -> *JSWorld
afterRender serverDirectory config me _ w
	# (afterRender`,w) = jsWrapFun (afterRender` config me) me w
	# w = addJSFromUrl True (serverDirectory +++ "split.js/split.min.js") (?Just afterRender`) me w
	= w

afterRender` :: !ResizeableSplit !JSVal !{!JSVal} !*JSWorld -> *JSWorld
afterRender` config me _ w
	// Safety: it appears that afterRender is called too often in some cases. Do
	// nothing if we have already created a Split instance.
	# (curSplit,w) = me .# "split" .? w
	| not (jsIsUndefined curSplit)
		= w
	// Create split instance
	# (hasHorizontal,w) = (me .# "domEl.classList.contains" .$ "itasks-horizontal") w
	  horizontal = fromJS False hasHorizontal
	# (createGutter,w) = jsWrapFunWithResult (createGutter horizontal) me w
	# (onDragEnd,w) = jsWrapFun onDragEnd me w
	# options = catMaybes
		[ ?Just $ "direction" :> if horizontal "horizontal" "vertical"
		, ?Just $ "minSize" :> either toJS toJS (fromMaybe (Left 0) config.minSize)
		, ((:>) "maxSize" o either toJS toJS) <$> config.maxSize
		, ?Just $ "expandToMin" :> config.expandToMin
		, ?Just $ "gutterSize" :> config.gutterSize
		, ?Just $ "gutterAlign" :> case config.gutterAlign of
			GutterStart -> "start"
			GutterCenter -> "center"
			GutterEnd -> "end"
		, ?Just $ "snapOffset" :> fromMaybe 0 config.snapOffset
		, ?Just $ "dragInterval" :> config.dragInterval
		, ((:>) "cursor" o if horizontal fst snd) <$> config.cursor
		, ?Just $ "gutter" :> createGutter
		, const ("onDragEnd" :> onDragEnd) <$> config.storeSizesId
		]
	# (mbSizes,w) = case config.storeSizesId of
		?None
			-> (?None, w)
		?Just _
			# (stored,w) = (jsGlobal "localStorage.getItem" .$ localStorageId) w
			-> (fromJSON (fromString (fromJS "" stored)), w)
	# mbSizes = mbSizes <|> config.sizePercentages
	# optionsWithSizes = maybe options (\sizes -> ["sizes" :> sizes:options]) mbSizes
	# (split,w) = (jsGlobal "Split" .$ (me .# "domEl.childNodes", jsRecord optionsWithSizes)) w
	# w = (me .# "split" .= split) w

	// Override onChangeUI to first remove the gutters and then re-add them, to
	// deal with dynamically added, removed, and replaced children.
	# (onChangeUI,w) = me .# "onChangeUI" .? w
	# (onChangeUI,w) = jsWrapFunWithResult (newOnChangeUI onChangeUI options me) me w
	# w = (me .# "onChangeUI" .= onChangeUI) w

	= w
where
	createGutter horizontal {[0]=index,[1]=direction} w
		# (div,w) = (jsDocument .# "createElement" .$ "div") w
		# w = (div .# "className" .= "gutter gutter-" +++ fromJS "" direction) w
		# w = case config.cursor of
			?Just (hor,ver) -> (div .# "style.cursor" .= if horizontal hor ver) w
			?None -> w
		= (div,w)

	localStorageId = "itasks-resizeable-split-" +++ fromJust config.storeSizesId

	onDragEnd {[0]=sizes} w
		# (sizes,w) = jsValToList` sizes (fromJS 0) w
		# w = (jsGlobal "localStorage.setItem" .$! (localStorageId, toString (toJSON sizes))) w
		= w

	newOnChangeUI onChangeUI options me {[0]=attributeChanges,[1]=childChanges} w
		# (sizes,w) = (me .# "split.getSizes" .$ ()) w
		# w = (me .# "split.destroy" .$! ()) w
		# (promise,w) = (onChangeUI .# "call" .$ (me, attributeChanges, childChanges)) w
		# (restoreSplit,w) = jsWrapFun (restoreSplit sizes) me w
		= (promise .# "then" .$ restoreSplit) w
	where
		restoreSplit sizes _ w
			# (split,w) = (jsGlobal "Split" .$ (me .# "domEl.childNodes", jsRecord ["sizes" :> sizes : options])) w
			# w = (me .# "split" .= split) w
			= w
