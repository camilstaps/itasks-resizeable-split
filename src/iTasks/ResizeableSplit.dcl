definition module iTasks.ResizeableSplit

/**
 * This file is part of iTasks-Resizeable-Split.
 *
 * iTasks-Resizeable-Split is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * iTasks-Resizeable-Split is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with iTasks-Resizeable-Split. If not, see
 * <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Either import :: Either

from iTasks.UI.Tune import class tune
from iTasks.WF.Definition import :: Task

:: ResizeableSplit =
	{ sizePercentages :: !?[Int]
		//* A list of sizes for each sub-UI. There should be as many elements
		//* as there are sub-UIs, and the total should add up to 100. By
		//* default, sub-UIs are given equal space.
	, minSize         :: !?(Either Int [Int])
		//* The minimum size of each sub-UI. When `Left`, the minimum size is
		//* the same for each sub-UI; with `Right`, each sub-UI can be given
		//* its own `minSize`. Defaults to 0.
	, maxSize         :: !?(Either Int [Int])
		//* Like `minSize`, but gives the maximum size. Defaults to infinity.
	, expandToMin     :: !Bool
		//* If `True`, `minSize` will override `sizePercentages`.
	, gutterSize      :: !Int
		//* Size of the gutter in pixels.
	, gutterAlign     :: !GutterAlign
		//* Where the size needed for the gutter should be taken from. If
		//* `GutterCenter` (the default), each neighboring UI loses some of its
		//* width/height; if `GutterStart`, only the preceding element; if
		//* `GutterEnd`, only the following element.
	, snapOffset      :: !?Int
		//* Number of pixels used to snap gutters to other gutters and the
		//* bounding box. Disabled when `?None`; defaults to 30.
	, dragInterval    :: !Int
		//* Number of pixels to drag gutters at once. Defaults to 1 (smooth
		//* dragging).
	, cursor          :: !?(String, String)
		//* The CSS cursor used for gutters. When set, the first element is the
		//* cursor for horizontal layouts and the second element the one for
		//* vertical layouts. Defaults to `("col-resize", "row-resize")`.
	, storeSizesId    :: !?String
		//* If set, uses this identifier to store the sizes of the sub-UIs in
		//* `LocalStorage` and retrieve them on a page reload. If sizes are set
		//* in `LocalStorage`, they override `sizePercentages`. The identifier
		//* should be unique in each app.
	}

:: GutterAlign = GutterStart | GutterCenter | GutterEnd

ResizeableSplit :==
	{ sizePercentages = ?None
	, minSize         = ?None
	, maxSize         = ?None
	, expandToMin     = False
	, gutterSize      = 5
	, gutterAlign     = GutterCenter
	, snapOffset      = ?Just 30
	, dragInterval    = 1
	, cursor          = ?Just ("col-resize", "row-resize")
	, storeSizesId    = ?None
	}

instance tune ResizeableSplit (Task a)
